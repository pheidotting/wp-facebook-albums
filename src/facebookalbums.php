<?php
    
    /**
     Plugin Name: Facebook fotoalbums in Wordpress
     Plugin URI: https://gitlab.com/pheidotting/wp-facebook-albums
     Description:  Toon alle Facebook albums van een pagina
     Version: 0.11.0
     Author: Patrick Heidotting
     Author URI: http://heidotting.nl/
     License: GPL2
     */
     function toon_albums($atts = []) {
        $accesscode = $atts['accesscode'];
        $pageId = $atts['pageid'];
        $exclude = explode(',', $atts['exclude']);

        if(isset($_GET['albumid'])){
            wp_enqueue_script('lightbox', plugins_url(dirname(plugin_basename(__FILE__))) . '/lightbox/js/lightbox.js', array('jquery'), "1.3.4", true);
            wp_enqueue_style('lightbox', plugins_url(dirname(plugin_basename(__FILE__))) . '/lightbox/css/lightbox.css', array(), "1.0");

            $albumid =  $_GET['albumid'];

            $album_response = wp_facebook_get("https://graph.facebook.com/$albumid?access_token=$accesscode&fields=name,description");

            echo '<a href="' . get_permalink() . '" title="Terug">Terug</a><br />';

            echo '<h4>' . $album_response['name'] . '</h4>';
            echo $album_response['description'] . '<br /><br />';
            $response = wp_facebook_get("https://graph.facebook.com/$albumid/photos?access_token=$accesscode&fields=images,name,created_time,picture,id");
            $fotos = $response['data'];

            while (isset($response['paging']['next'])){
                $response = wp_facebook_get($response['paging']['next']);
                $fotos = array_merge($response['data'], $fotos);
            }
            $fotosmetdatum = null;
            $i = 0;
            foreach($fotos as $foto) {
                $f = null;
                $naam = '';
                if(isset($foto['name'])) {
                    $naam = $foto['name'];
                }
                $f['thumb'] = $foto['picture'];
                $f['id'] = $foto['id'];
                $f['naam'] = $naam;
                foreach($foto['images'] as $image){                    
                    if($image['width'] == 900) {
                        $f['source'] = $image['source'];
                    }
                }
                
                if(!isset($f['source'])){
                    foreach($foto['images'] as $image){                    
                        if($image['width'] == 600) {
                            $f['source'] = $image['source'];
                        }
                    }
                }
                
                if(!isset($f['source'])){
                    foreach($foto['images'] as $image){                    
                        $f['source'] = $image['source'];
                    }
                }
                $fotosmetdatum[substr($foto['created_time'], 0 ,23) . $i++] = $f;
            }
            arsort($fotosmetdatum);
            foreach($fotosmetdatum as $foto) {
                echo '<a href="' . $foto['source'] . '" data-lightbox="image-1" data-title="' . $foto['naam'] . '"><img src="' . $foto['thumb'] . '" alt="' . $foto['naam'] . '" style="display: inline; margin: 10px;" /></a>';
            }

            //
        } else {
            $albums0 = wp_facebook_get("https://graph.facebook.com/$pageId/albums?access_token=$accesscode");

            $alb = null;
            foreach ($albums0 as $albums) {
                foreach ($albums as $album) {
                    if(!is_string($album)){
                        if(isset($album['id']) && !in_array($album['id'], $exclude)){
                            $alb[substr($album['created_time'], 0, 10)] = $album;
                        }
                    }
                }
            }

            arsort($alb);

            $jaar = null;
            foreach ($alb as $album) {
                if($jaar != substr($album['created_time'], 0, 4)) {
                    $jaar = substr($album['created_time'], 0, 4);
                    echo '<h3>' . $jaar . '</h4>';
                }
                $vraagtekenOfAmpersand = '&';
                if(strpos(get_permalink(), '?') == 0){
                    $vraagtekenOfAmpersand = '?';
                }

                $datum = substr($album['created_time'], 0, 10);
                $dag = substr($datum, 8, 2);
                $maand = substr($datum, 5, 2);
                $jaar = substr($datum, 0, 4);
                $datum = $dag . '-' . $maand . '-' . $jaar;

                echo '<a href="' . get_permalink() . $vraagtekenOfAmpersand .'albumid='. $album['id'] . '" title="' . $album['name'] . '">'.$album['name'] . ' (' . $datum . ')</a><br />';
            }
        }

        ob_start();

        return ob_get_clean();
    }

    function wp_facebook_get($url)    {
        //Try to access the URL
        $result = wp_remote_get($url, array('sslverify' => false));

        //In some rare situations, Wordpress may unexpectedly return WP_Error.  If so, I'll create a Facebook-style error object
        //so my Facebook-style error handling will pick it up without special cases everywhere.
        if (is_wp_error($result)) {
            $result->error->message = "wp_remote_get() failed!";
            if (method_exists($result, 'get_error_message')) $result->error->message .= " Message: " . $result->get_error_message();
            return $result;
        }

        //Otherwise, we're OK - decode the JSON text provided by Facebook into a PHP object.
        return json_decode($result['body'], true);
    }


    add_shortcode('facebook_album', 'toon_albums');
?>
